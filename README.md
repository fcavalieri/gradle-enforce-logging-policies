# Gradle Enforce Logging Policies

## Usage

settings.gradle
```groovy
pluginManagement {
    repositories {
        maven { url "https://gitlab.com/api/v4/projects/37542456/packages/maven" }
        gradlePluginPortal()
    }
}
```

build.gradle
```groovy
plugins {
    id "com.fcavalieri.gradle.enforceloggingpolicies" version 'X.X.X'
}
```

Usage:

```groovy
enforceLoggingPolicies {
   library = true
}
```

Defined tasks:
* enforceLoggingPolicies
