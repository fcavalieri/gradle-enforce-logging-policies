package com.fcavalieri.gradle

import groovy.transform.Internal
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.tasks.TaskAction


class EnforceLoggingPoliciesPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.extensions.create("enforceLoggingPolicies", EnforceLoggingPoliciesExtension, project)
        project.tasks.register("enforceLoggingPolicies", EnforceLoggingPoliciesTask)
    }
}

class EnforceLoggingPoliciesExtension {
    Boolean library

    @Internal
    Project project

    EnforceLoggingPoliciesExtension(Project project) {
        this.project = project
    }

    boolean isLibrary() {
        if (library != null)
            return library
        throw new RuntimeException("Undefined library property for enforceLoggingPolicies")
    }
}

class EnforceLoggingPoliciesTask extends DefaultTask {
    @TaskAction
    void enforceLoggingPolicies() {
        if (project.getConfigurations() == null || project.getConfigurations().size() == 0 ||
            project.getConfigurations().findByName("runtimeClasspath") == null ||
            project.getConfigurations().findByName("runtimeClasspath").getAllDependencies() == null ||
            project.getConfigurations().findByName("runtimeClasspath").getAllDependencies().size() == 0
        ) {
            print("Ignored")
            return
        }

        def isLibraryBool = project.enforceLoggingPolicies.isLibrary()
        def exclusionsA = project.getConfigurations()
                .getByName("runtimeClasspath")
                .getAllDependencies()
                .findAll { it instanceof org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency }
                .collect {((org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency)it) }
                .collect { it.getExcludeRules()}
                .flatten()
                .collect {((org.gradle.api.internal.artifacts.DefaultExcludeRule)it) } //We do not want to miss the other
        def exclusionsB = project.getConfigurations()
                .getByName("runtimeClasspath")
                .getExcludeRules()
                .collect {((org.gradle.api.internal.artifacts.DefaultExcludeRule)it) }
        def exclusions = exclusionsA + exclusionsB
        def directDeps = project.getConfigurations()
                .getByName("runtimeClasspath")
                .getAllDependencies()
                .findAll { it instanceof org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency }
                .collect {((org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency)it) }
        def directDepsNames = directDeps
                .collect {it.getModule().toString()}
                .toSet()
        def allDeps = project.getConfigurations()
                .getByName("runtimeClasspath")
                .incoming
                .getResolutionResult()
                .getAllDependencies()
                .collect {d -> d.getRequested() }
                .findAll {it instanceof org.gradle.internal.component.external.model.DefaultModuleComponentSelector}
        def allDepsNames = allDeps
                .collect {m -> ((org.gradle.internal.component.external.model.DefaultModuleComponentSelector)m).getModuleIdentifier().toString()}
                .toSet()

        /*
         * No logging implementation is present in transitive dependencies of libraries.
         * Only logback is present in transitive dependencies of applications.
         */
        if (allDepsNames.contains("log4j:log4j")) {
            throw new RuntimeException("log4j 1.x leaked into runtime classpath")
        }
        if (allDepsNames.contains("commons-logging:commons-logging")) {
            throw new RuntimeException("commons-logging leaked into runtime classpath")
        }
        if (allDepsNames.contains("org.apache.logging.log4j:log4j-core")) {
            throw new RuntimeException("log4j-core leaked into runtime classpath")
        }
        if (allDepsNames.contains("org.jboss.logmanager:jboss-logmanager")) {
            throw new RuntimeException("jboss-logmanager leaked into runtime classpath")
        }
        if (isLibraryBool && allDepsNames.contains("ch.qos.logback:logback-core")) {
            throw new RuntimeException("logback-core leaked into runtime classpath of a library")
        }
        if (!isLibraryBool && !allDepsNames.contains("ch.qos.logback:logback-core")) {
            throw new RuntimeException("logback-core is missing from the runtime classpath of an application")
        }

        /*
         * Additionally log4j-api is not a direct dependency.
         * It is a transitive dependency of log4j-to-slf4j,
         * so it cannot be excluded in the transitive dependencies.
         */
        if (directDepsNames.contains("org.apache.logging.log4j:log4j-api")) {
            throw new RuntimeException("Direct dependency on log4j-api")
        }
        /*
         * Additionally jboss-logging is not a direct dependency.
         * We do not replace this, but rely on its redirection to slf4j
         */
        if (directDepsNames.contains("org.jboss.logging:jboss-logging")) {
            throw new RuntimeException("Direct dependency on jboss-logging")
        }

        def isLog4JPresent = allDepsNames.contains("org.apache.logging.log4j:log4j-api")
        def isLog4JRedirected = allDepsNames.contains("org.apache.logging.log4j:log4j-to-slf4j")
        if (isLog4JPresent && !isLog4JRedirected) {
            throw new RuntimeException("Log4J API is present but not redirected to slf4j")
        } else if (!isLog4JPresent && isLog4JRedirected) {
            throw new RuntimeException("Log4J redirected to slf4j but log4j api is not present")
        }

        def isSLF4JPresent = allDepsNames.contains("org.slf4j:slf4j-api")

        def isJCLExcluded = exclusions.any { "commons-logging".equals(it.getGroup()) || "commons-logging".equals(it.getModule()) }
        def isJCLReplacedSomewhere = allDepsNames.contains("org.slf4j:jcl-over-slf4j")
        def isJCLReplacedHere = directDepsNames.contains("org.slf4j:jcl-over-slf4j")
        if (isJCLExcluded && !isJCLReplacedSomewhere) {
            throw new RuntimeException("JCL was excluded but not replaced")
        } else if (!isJCLExcluded && isJCLReplacedHere) {
            throw new RuntimeException("JCL was replaced but not excluded")
        }

        def isJBossLoggingUsed = allDepsNames.contains("org.jboss.logging:jboss-logging")

        println("Loggers report:")
        if (isSLF4JPresent) {
            println(" * SLF4J")
        }
        if (isJCLReplacedSomewhere) {
            println(" * JCL -> SLF4J")
        }
        if (isLog4JRedirected) {
            println(" * Log4j2 -> SLF4J")
        }
        if (isJBossLoggingUsed) {
            println(" * JBoss -> SLF4J")
        }
    }
}

